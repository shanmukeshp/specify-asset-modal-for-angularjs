export default class SpecifyAssetModalConfig {

    _idsOfAssetsToFilter:Array<string>;

    _saveCallback;

    constructor(idsOfAssetsToFilter:Array<string>,
                saveCallback) {

        if (!idsOfAssetsToFilter) {
            throw 'idsOfAssetsToFilter required';
        }
        this._idsOfAssetsToFilter = idsOfAssetsToFilter;

        if (!saveCallback) {
            throw 'saveCallback required';
        }
        this._saveCallback = saveCallback;

    }

    get idsOfAssetsToFilter():Array<string> {
        return this._idsOfAssetsToFilter;
    }

    get saveCallback() {
        return this._saveCallback;
    }

}
