import SessionManager from 'session-manager';
import AssetServiceSdk from 'asset-service-sdk';
import ProductGroupServiceSdk from 'product-group-service-sdk';
import ProductLineServiceSdk from 'product-line-service-sdk';
import SpecifyAssetModalConfig from './specifyAssetModalConfig';
import {AddAssetModalConfig} from 'add-asset-modal';

export default class Controller {

    _$q;

    _sessionManager:SessionManager;

    _$modalInstance;

    _assetServiceSdk:AssetServiceSdk;

    _addAssetModal;

    _config:SpecifyAssetModalConfig;

    _selectedComponents = [];

    _selectedAsset;

    _productGroupHashTable;

    _productLineHashTable;

    _assetList;

    _filteredAssetList;

    _assetFilter = {serialNumber: null, productGroupId: null};

    _serialNumberOfAssetToAdd;

    constructor(sessionManager:SessionManager,
                $q,
                $modalInstance,
                assetServiceSdk:AssetServiceSdk,
                productGroupServiceSdk:ProductGroupServiceSdk,
                productLineServiceSdk:ProductLineServiceSdk,
                addAssetModal,
                config:SpecifyAssetModalConfig) {

        if (!sessionManager) {
            throw new TypeError('sessionManager required');
        }
        this._sessionManager = sessionManager;

        if (!$q) {
            throw new TypeError('$q required');
        }
        this._$q = $q;

        if (!$modalInstance) {
            throw new TypeError('$modalInstance required');
        }
        this._$modalInstance = $modalInstance;

        if (!assetServiceSdk) {
            throw new TypeError('assetServiceSdk required');
        }
        this._assetServiceSdk = assetServiceSdk;

        if (!productGroupServiceSdk) {
            throw  new TypeError('productGroupServiceSdk required');
        }

        if (!productLineServiceSdk) {
            throw new TypeError('productLineServiceSdk required');
        }

        if (!addAssetModal) {
            throw new TypeError('addAssetModal required');
        }
        this._addAssetModal = addAssetModal;

        if (!config) {
            throw new TypeError('config required');
        }
        this._config = config;

        $q(
            resolve =>
                sessionManager
                    .getAccessToken()
                    .then(accessToken => resolve(accessToken))
        )
            .then(accessToken =>
                $q.all([
                    // init product groups
                    productGroupServiceSdk
                        .listProductGroups(accessToken)
                        .then(productGroupList => {
                            this._productGroupHashTable =
                                productGroupList
                                    .reduce(
                                        (hashTable, productGroup) => {
                                            hashTable[productGroup.id] = productGroup;
                                            return hashTable;
                                        },
                                        {}
                                    );
                        }),
                    // init product lines
                    productLineServiceSdk
                        .listProductLines(accessToken)
                        .then(productLineList => {
                            this._productLineHashTable =
                                productLineList
                                    .reduce(
                                        (hashTable, productLine) => {
                                            hashTable[productLine.id] = productLine;
                                            return hashTable;
                                        },
                                        {}
                                    );
                        }),
                    // init assets
                    sessionManager
                        .getUserInfo()
                        .then(userInfo =>
                            assetServiceSdk
                                .listAssetsWithAccountId(
                                    userInfo.account_id,
                                    accessToken
                                )
                        )
                        .then(assetList => {
                            this._assetList = assetList;
                            this._filteredAssetList = assetList;
                            this.filterAssets();
                        })
                ])
            );

    }

    get selectedComponents() {
        return this._selectedComponents;
    }

    get selectedAsset() {
        return this._selectedAsset;
    }

    set selectedAsset(value) {
        this._selectedAsset = value;
    }

    get productGroupHashTable() {
        return this._productGroupHashTable;
    }

    get productLineHashTable() {
        return this._productLineHashTable;
    }

    get filteredAssetList() {
        return this._filteredAssetList;
    }

    get assetFilter() {
        return this._assetFilter;
    }

    get serialNumberOfAssetToAdd() {
        return this._serialNumberOfAssetToAdd;
    }

    set serialNumberOfAssetToAdd(value) {
        this._serialNumberOfAssetToAdd = value;
    }

    addAssetViaSerialNumberOrShowAddNotListedModal() {

        let accessToken;
        let userInfo;
        this._$q(resolve =>
                resolve(
                    Promise.all([
                        this._sessionManager
                            .getAccessToken()
                            .then(_accessToken => {
                                accessToken = _accessToken;
                            }),
                        this._sessionManager
                            .getUserInfo()
                            .then(_userInfo => {
                                userInfo = _userInfo;
                            })
                    ])
                )
            )
            .then(() =>
                this._assetServiceSdk
                    .searchForAssetWithSerialNumber(
                        this._serialNumberOfAssetToAdd,
                        accessToken
                    )
            )
            .then(assetOrNull => {
                    if (!assetOrNull) {
                        // if asset with provided serial number is not know to system,
                        // fallback to manual entry
                        return this._addAssetModal
                            .show(
                                new AddAssetModalConfig(
                                    this._serialNumberOfAssetToAdd,
                                    userInfo.account_id
                                )
                            )
                            .then(assetId => this._assetServiceSdk
                                .getAssetsWithIds(
                                    [assetId],
                                    accessToken
                                )
                                .then(assets => assets[0]));
                    } else {
                        return assetOrNull;
                    }
                }
            )
            .then(asset => {

                    this._assetList.push(asset);

                    this._serialNumberOfAssetToAdd = null;

                    this.filterAssets();
                }
            );
    }

    cancel() {
        this._$modalInstance.dismiss();
    }

    filterAssets() {
        this._filteredAssetList =
            this._assetList.filter(asset => {

                // apply config.idsOfAssetsToFilter
                if (this._config.idsOfAssetsToFilter.some(element => element == asset.id)) {
                    return false;
                }

                // apply product group filter if present
                if (this._assetFilter.productGroupId) {
                    if (
                        this._productLineHashTable[asset.productLineId].productGroupId
                        != this._assetFilter.productGroupId
                    ) {
                        return false;
                    }
                }

                // apply serial number filter if present
                if (this._assetFilter.serialNumber) {
                    if (
                        asset.serialNumber.toUpperCase().indexOf(this._assetFilter.serialNumber.toUpperCase())
                        !== 0
                    ) {
                        return false;
                    }
                }

                // filter out selectedComponents
                return !this._selectedComponents.some(element => element.id == asset.id);

            })
    }

    addComponent(component) {

        this._selectedComponents.push(component);
        this._selectedAsset = null;
        this.filterAssets();

    }

    removeComponent(component) {

        this._selectedComponents =
            this._selectedComponents.filter(item => {
                return component.id != item.id;
            });

        this.filterAssets();

    }

    saveAndClose() {

        if (this._selectedComponents.length > 0) {

            this._config.saveCallback(this._selectedComponents);

        }
        else {

            this._config.saveCallback(this._selectedAsset);

        }

        this._$modalInstance.close();

    }

    saveAndAddAnother() {

        if (this._selectedComponents.length > 0) {

            this._selectedComponents.forEach(element => {
                this._config.idsOfAssetsToFilter.push(element.id);
            });

            this._config.saveCallback(this._selectedComponents);

            this._selectedComponents = [];

        }
        else {

            this._config.idsOfAssetsToFilter.push(this._selectedAsset.id);

            this._config.saveCallback([this._selectedAsset]);

            this._selectedAsset = null;

        }

        this.filterAssets();

    }
}

Controller.$inject = [
    'sessionManager',
    '$q',
    '$modalInstance',
    'assetServiceSdk',
    'productGroupServiceSdk',
    'productLineServiceSdk',
    'addAssetModal',
    'config'
];