import Controller from './controller';
import './default.css!css';
import template from './index.html!text';
import SpecifyAssetModalConfig from './specifyAssetModalConfig';

export default class SpecifyAssetModal {

    _$modal;

    constructor($modal) {

        if (!$modal) {
            throw new TypeError('$modal required');
        }
        this._$modal = $modal;

    }

    /**
     * Shows the specify asset modal
     * @param {SpecifyAssetModalConfig} config
     * @returns {Promise}
     */
    show(config:SpecifyAssetModalConfig):Promise {

        return this._$modal
            .open(
                {
                    controller: Controller,
                    controllerAs: 'controller',
                    template: template,
                    backdrop: 'static',
                    resolve: {
                        config: () => config
                    }
                }
            )
            .result;
    }
}

SpecifyAssetModal.$inject = [
    '$modal'
];