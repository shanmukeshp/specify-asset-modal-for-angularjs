import angular from 'angular';
import '../src/index';
import RouteConfig from './routeConfig';
import SessionManager,{SessionManagerConfig} from 'session-manager';
import AssetServiceSdk,{AssetServiceSdkConfig} from 'asset-service-sdk';
import ProductLineServiceSdk,{ProductLineServiceSdkConfig} from 'product-line-service-sdk'
import ProductGroupServiceSdk,{ProductGroupServiceSdkConfig} from 'product-group-service-sdk'
import 'angular-route';

const precorConnectApiBaseUrl = 'https://api-dev.precorconnect.com';

const sessionManager =
    new SessionManager(
        new SessionManagerConfig(
            precorConnectApiBaseUrl/* identityServiceBaseUrl */,
            'https://precor.oktapreview.com/app/template_saml_2_0/exk5cmdj3pY2eT5JU0h7/sso/saml'/* loginUrl */,
            'https://dev.precorconnect.com/customer/account/logout/'/* logoutUrl*/,
            30000/* accessTokenRefreshInterval */
        )
    );

const assetServiceSdk =
    new AssetServiceSdk(
        new AssetServiceSdkConfig(
            precorConnectApiBaseUrl
        )
    );

const productLineServiceSdk =
    new ProductLineServiceSdk(
        new ProductLineServiceSdkConfig(
            precorConnectApiBaseUrl
        )
    );

const productGroupServiceSdk =
    new ProductGroupServiceSdk(
        new ProductGroupServiceSdkConfig(
            precorConnectApiBaseUrl
        )
    );

angular
    .module(
        'exampleApp.module',
        [
            'ngRoute',
            'specifyAssetModal.module'
        ]
    )
    .constant(
        'sessionManager',
        sessionManager
    )
    .constant(
        'assetServiceSdk',
        assetServiceSdk
    )
    .constant(
        'productLineServiceSdk',
        productLineServiceSdk
    )
    .constant(
        'productGroupServiceSdk',
        productGroupServiceSdk
    )
    .config(
        [
            '$routeProvider',
            $routeProvider => new RouteConfig($routeProvider)
        ]
    );
